cd /home/pi

echo "stopping tmonitor"
sudo service tmonitor stop

echo "removing old version"
rm -rf /home/pi/temp/tmonitor.zip

echo "downloading new tmonitor"
sudo wget https://gitlab.com/m.gamperl/tmonitor/repository/archive.zip?ref=stable -O /home/pi/temp/tmonitor.zip
sudo unzip -o -d /home/pi/temp/tmonitor /home/pi/temp/tmonitor.zip
sudo cp -rf /home/pi/temp/tmonitor/tmonitor-*/* /home/pi/tmonitor
sudo rm -rf /home/pi/temp/tmonitor/tmonitor-*

echo "installing dependencies"
sudo rm -rf /home/pi/tmonitor/node_modules
cd /home/pi/tmonitor
sudo npm install
cd /home/pi

echo "updating init.d script"
sudo cp /home/pi/tmonitor/src/init.d /etc/init.d/tmonitor
sudo chmod a+x /etc/init.d/tmonitor

echo "starting service"
sudo service tmonitor start
