#!/bin/bash
cd /home/pi

echo "installing unzip"
sudo apt-get install unzip

echo "creating /home/pi/temp"
sudo mkdir -p /home/pi/temp
cd /home/pi/temp

echo "creating /home/pi/tmonitor"
sudo mkdir -p /home/pi/tmonitor

echo "downloading node"
wget https://nodejs.org/dist/v4.0.0/node-v4.0.0-linux-armv7l.tar.gz
tar -xvf node-v4.0.0-linux-armv7l.tar.gz
#wget http://node-arm.herokuapp.com/node_latest_armhf.deb
echo "installing node"
cd node-v4.0.0-linux-armv7l
sudo cp -R * /usr/local/

cd /home/pi/temp
#sudo dpkg -i node_latest_armhf.deb

echo "node installed"
node -v
which node
which npm

echo "export path"
export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

echo "installing forever"
sudo npm install -g forever

echo "downloading tmonitor"
sudo wget https://gitlab.com/m.gamperl/tmonitor/repository/archive.zip?ref=stable -O /home/pi/temp/tmonitor.zip
sudo unzip -o -d /home/pi/temp/tmonitor /home/pi/temp/tmonitor.zip
sudo cp -rf /home/pi/temp/tmonitor/tmonitor-*/* /home/pi/tmonitor
sudo rm -rf /home/pi/temp/tmonitor/tmonitor-*

echo "creating init.d script"
sudo cp /home/pi/tmonitor/src/init.d /etc/init.d/tmonitor
sudo chmod a+x /etc/init.d/tmonitor
sudo update-rc.d tmonitor defaults

echo "installing tmonitor dependencies"
cd /home/pi/tmonitor
sudo npm install

cd /home/pi

echo "{}" > /home/pi/tmonitor/sensors.json

echo "starting service"
sudo service tmonitor start
