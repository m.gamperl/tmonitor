
import {SensorApi} from "../src/lib/SensorApi";
import path from "path";

require("chai").should();

describe('sensorApi', function() {

  var testConfig = {
    "sensorFolder" : "./test/data/sensors",
    "sensorRegexp" : "^(28)(.*)",
    "sensorFileName" : "w1_slave",
    "database" : "./test/data/db/tmonitor",
    "sensorFile" : "./test/data/sensors.json",
    "logLevel" : "warn"
  };

  var sensorApi = new SensorApi(testConfig);

  it("getSensorFile",()=>{
    var expected = path.join(path.resolve(process.cwd(),testConfig.sensorFolder),"1234",testConfig.sensorFileName);
    sensorApi.getSensorFile("1234").should.equal(expected);
  });

  it('readTemperature', async () => {
    var temp = await sensorApi.readTemperature("28_1");
    temp.should.equal(13.467);
  });


  it("getSensorIds", async () => {
    var ids = await sensorApi.getSensorIds();
    ids.should.eql(["28_1","28_2","28_3"]);
  });

  it("loadSensors", async () => {
    var expected = {
      "28_1": {
        "id": "28_1",
        "name": "test",
        "temperature": -99
      },
      "28_2": {
        "id": "28_2",
        "name": "test2",
        "temperature": -99
      },
      "28_3": {
        "id": "28_3",
        "name": "28_3",
        "temperature": -99
      }
    };
    var sensors = await sensorApi.loadSensors(sensorApi.sensorFile);
    sensors.should.deep.equal(expected);
  });

  it("load and save sensor files", async () => {
    var data = {test : "data"};
    await sensorApi.saveSensorsToFile("test/tmp-ignore.json",data);
    var result = await sensorApi.loadSensorsFromFile("test/tmp-ignore.json");
    result.should.deep.equal(data);
  });

});
