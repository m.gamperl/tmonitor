require("chai").should();
var expect = require("chai").expect;

import {Config} from "../src/lib/Config";

describe('config', function() {


  it('getEnvValue', function() {

    var config = new Config();

    var env = {
      TMONITOR_test : "testValue",
      TMONITOR_test2 : "testValue2"
    }

    config.getEnvValue("test",env).should.equal("testValue");
    config.getEnvValue("test2",env).should.equal("testValue2");
    expect(config.getEnvValue("test3",env)).to.be.undefined;

  });

});
