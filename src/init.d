#!/bin/sh

export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules
export TMONITOR_PORT=80

case "$1" in
  start)
  exec sudo forever start --append --workingDir=/home/pi/tmonitor --sourceDir=/home/pi/tmonitor/src --pidFile /var/run/tmonitor.pid -l /var/log/tmonitor.log -o /var/log/tmonitor.out.log -e /var/log/tmonitor.err.log index.js
  ;;

  stop)
  exec sudo forever stop 0
  ;;
esac

exit 0
