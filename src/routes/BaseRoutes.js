import express from "express";

export class BaseRoutes {
  constructor(){
      this.app = express();
  }

  getApp(){
    return this.app;
  }

}
