import {BaseRoutes} from "./BaseRoutes";
import {Exporter} from "../lib/Export";

export class HistoryRecordRoutes extends BaseRoutes{

  constructor(_config,_sensorApi,_db){
    super();
    this.config = _config;
    this.sensorApi = _sensorApi;
    this.db = _db;
    this.exporter = new Exporter(_config);

    this.app.get("/historyRecords",async (req,res,next)=>{
      var result = await this.db.getAllHistoryRecords();
      this.sendCSV(result,res);
    });

    this.app.get("/historyRecords/currentMonth",async (req,res,next)=>{
      var result = await this.db.getHistoryRecordsForCurrentMonth();
        this.sendCSV(result,res);
    });

    this.app.get("/historyRecords/lastMonth",async (req,res,next)=>{
      var result = await this.db.getHistoryRecordsForLastMonth();
        this.sendCSV(result,res);
    });

    this.app.get("/api/historyRecords",async (req,res,next) =>{
      var result = await this.db.getAllHistoryRecords();
      res.json(result);
    });

    this.app.get("/api/historyRecords/lastMonth",async (req,res,next) =>{
      var result = await this.db.getHistoryRecordsForLastMonth();
      res.json(result);
    });

    this.app.get("/api/historyRecords/currentMonth",async (req,res,next) =>{
      var result = await this.db.getHistoryRecordsForCurrentMonth();
      res.json(result);
    });

    this.app.get("/api/clearDatabase",async (req,res,next)=>{
      var result = await this.db.clearDatabase();
      res.json(result);
    });

  }


      sendCSV(result,res){
        if(result.status === "ok"){
          this.exporter.sendCSV(result.result,this.sensorApi.getSensors(),res);
        }else{
          res.json(result);
        }
      }


};
