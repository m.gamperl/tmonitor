import {BaseRoutes} from "./BaseRoutes";

export class GpioRoutes extends BaseRoutes{

  constructor(_config,_gpioApi){
    super();
    this.config = _config;
    this.gpioApi = _gpioApi;

    this.app.get("/gpios",(req,res,next)=>{
      res.send(this.gpioApi.getGpios());
    });

    this.app.post("/gpios/:gpio",(req,res,next)=>{
      var value = req.query.value;
      var gpio = this.gpioApi.set(req.params.gpio,value);
      res.send(gpio);
    });

  }

};
