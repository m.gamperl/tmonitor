"use strict"
var express = require("express");
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var fs = require("fs");
var path = require("path");
var _ = require("lodash");
var cron = require('cron');

import bunyan from "bunyan";

import {Config} from "./lib/Config";
import {SensorApi} from "./lib/SensorApi";
import {GpioApi} from "./lib/GpioApi";

import {GpioRoutes} from "./routes/GpioRoutes";
import {HistoryRecordRoutes} from "./routes/HistoryRecordRoutes";

import {Scheduler} from "./jobs/Scheduler";
import {CheckTemperatureJob} from "./jobs/CheckTemperatureJob";
import {StoreDataJob} from "./jobs/StoreDataJob";
import {Db} from "./lib/Db";

var config = new Config();
var logger = bunyan.createLogger({name : "tmonitor", level : config.getConfig().logLevel});
var db = new Db(config.getConfig());
var sensorApi = new SensorApi(config.getConfig());
var gpioApi = new GpioApi(config.getConfig());
var scheduler = new Scheduler(config.getConfig());

var gpioRoutes = new GpioRoutes(config.getConfig(),gpioApi);
var historyRecordRoutes = new HistoryRecordRoutes(config.getConfig(),sensorApi,db);

app.use(express.static('./src/public'));

app.use(gpioRoutes.getApp());
app.use(historyRecordRoutes.getApp());

io.on("connection",async function(socket){
  logger.info("ui connected ", socket.id);

  socket.emit("sensors",{sensorList: _.values(sensorApi.getSensors())});
  socket.emit("gpios",{gpioList: _.values(gpioApi.getGpios())});

  socket.on("updateGpio",async function(data){
    logger.info("set pin ", data.id , " to ", data.value );
    gpioApi.update(data.id,data);
  });

  socket.on("changeSensorName",async function(data){
    if(data.name && data.name.length > 0){
      sensorApi.setName(data.id,data.name);
      io.emit("newSensorName",sensorApi.getSensor(data.id));
      await sensorApi.saveSensorsToFile();
    }
  });
});

async function start(){

  gpioApi.initGpios();
  config.printConfig();

  logger.info("sensors: ",sensorApi.getSensors());
  if(_.keys(sensorApi.getSensors()).length <= 0){
    logger.info("loading sensors");
    await sensorApi.loadSensors();
    logger.info("sensors loaded");
    new CheckTemperatureJob(config.getConfig(),sensorApi,io).execute();
    await sensorApi.saveSensorsToFile();
  }

  logger.info("init database");
  db.initDatabase();

  logger.info("starting jobs");
  scheduler.scheduleAndStart("CheckTemperatureJob",new CheckTemperatureJob(config.getConfig(),sensorApi,io));
  scheduler.scheduleAndStart("StoreDataJob",new StoreDataJob(config.getConfig(),sensorApi,db));

  server.listen(process.env.TMONITOR_SERVER_PORT || config.getConfig().port || 80);
}

start();


process.on('SIGINT', function () {
  gpioApi.close();
  process.exit();
});

process.on('unhandledRejection', function(reason, p){
    logger.error("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
    process.exit();
});
