import {LogClass} from "../lib/LogClass";
var cron = require('cron');

export class Scheduler extends LogClass {
    constructor(_config){
      super(_config);
      this.jobs = {};
    }

    async scheduleAndStart(name,job){
      this.jobs[name] = cron.job(job.getInterval(),()=>{
        this.log.info("Executing job: "+ name);
        job.execute();
      });
      this.jobs[name].start();
    }
}
