import {SimpleJob} from "./SimpleJob";

export class StoreDataJob extends SimpleJob {

  constructor(_config,_sensorApi,_db){
    super(_config);
    this.config = _config;
    this.sensorApi = _sensorApi;
    this.db = _db;
  }

  getInterval(){
    return this.config.storeDataInterval;
  }

  async execute(){
    var data = {
      _id : Date.now() + "",
      timestamp : Date.now(),
      datetime : new Date(),
      type : "historyRecord"
    };
    data.sensors = this.sensorApi.getSensors();
    this.log.info("storing sensor data: ", data.sensors);
    try {
      await this.db.storeData(data);
    }catch(e){
      this.log.error("can not save data!");
    }
  }

}
