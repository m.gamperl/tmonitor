import {SimpleJob} from "./SimpleJob";
import _ from "lodash";
import * as MathUtils from "../lib/MathUtils";

export class CheckTemperatureJob extends SimpleJob {

  constructor(_config,_sensorApi,_io){
    super(_config);
    this.config = _config;
    this.sensorApi = _sensorApi;
    this.io = _io;
  }

  getInterval(){
    return this.config.checkTemperatureInterval;
  }

  async execute(){
      // perform operation e.g. GET request http.get() etc.
      //console.log("checkTemperatureJob run");
      _.values(this.sensorApi.getSensors()).forEach(async (sensor)=>{

        var newTemp = await this.sensorApi.readTemperature(sensor.id);
        if(newTemp !== sensor.temperature){
          //console.log("new temperature: ", newTemp);
          this.sensorApi.setTemperature(sensor.id,newTemp);
          this.io.emit('newTemperature', { id: sensor.id, temperature : Math.round10(newTemp,-1)});
        }
      });
  }

}
