import _ from "lodash";
import path from "path"
import {LogClass} from "./LogClass";
import * as fsp from "./fsp";
import fs from "fs";

export class SensorApi extends LogClass {

  constructor(_config){
    super(_config);
    this.config = _config;
    this.sensors = {};
    this.sensorFile = path.resolve(process.cwd(),this.config.sensorFile);
  }

  getSensors(){
    return this.sensors;
  }

  getSensorFile(id) {
    return path.join(this.getSensorFolder(),id,this.config.sensorFileName);
  }

  getSensorFolder(){
    return path.resolve(process.cwd(),this.config.sensorFolder);
  }

  setName(id,name){
    this.getSensor(id).name = name;
  }

  setTemperature(id,temperature){
    this.getSensor(id).temperature = temperature;
  }

  getSensor(id){
    if(!this.sensors[id]){
      this.sensors[id] = {
        id : id,
        name : id,
        temperature : 0
      };
    }
    return this.sensors[id];
  }

  parseTemperature(content,id){
    var result = content.match(/[-]?[0-9]+$/gm);
    if(result && result.length >= 1){
      return result[0] / 1000;
    }else{
      throw new Error("Temperatur von SensorId '" + id + "' kann nicht gelesen werden");
      return -99;
    }
  }

  async readTemperature(id){
    this.log.info("reading sensor data: ", this.getSensorFile(id));
    try {
      var content = await fsp.readFile(this.getSensorFile(id));
      try {
        var temperature = this.parseTemperature(content,id);
        return temperature;
      }catch(parseError){
        throw parseError;
      }
    }catch(err){
      this.log.error("Error at readTemperature for sensorId ",id,": ", err);
      throw err;
    }
  }

  async getSensorIds() {
    try {
      var files = await fsp.readdir(this.getSensorFolder());
      var filtered = _.filter(files,(f) => {
        return f.match(new RegExp(this.config.sensorRegexp));
      });
      this.log.info("reading sensors from ", this.getSensorFolder(),new RegExp(this.config.sensorRegexp));
      return filtered;

    }catch(dirError){
      this.log.error("Error at getSensorIds readddir: ", dirError);
      throw dirError;
    }
  }

  async loadSensorsFromFile(sensorFile){
    try {
      await fsp.access(sensorFile,fs.constants.F_OK);
      var content = await fsp.readFile(sensorFile);
      return JSON.parse(content);
    } catch (e) {
      this.log.warn("Can not read sensors.json " + sensorFile, e);
      return {};
    }
  }

  async saveSensorsToFile(sensorFile,sensors){
    sensorFile = sensorFile || this.sensorFile;
    sensors = sensors || this.sensors;
    try {
      await fsp.writeFile(sensorFile,JSON.stringify(sensors,null,2));
    }catch(e){
      this.log.error("can not save sensors to file: ", e);
      throw e;
    }
  }

  async loadSensors(sensorFile){
    sensorFile = sensorFile || this.sensorFile;
    this.log.info("load sensors from file");

    var sensorsFromFile;

    try {
      sensorsFromFile = await this.loadSensorsFromFile(sensorFile);
    }catch(e){
      this.log.error("can not load sensors from file: "+ sensorFile, e);
      process.exit();
    }

    this.sensors = {};
    var connectedSensorIds = await this.getSensorIds();

    connectedSensorIds.forEach((id)=>{
      this.sensors[id] = {
        id : id,
        name : sensorsFromFile[id] ?  sensorsFromFile[id].name : id,
        temperature : -99
      };
    });

    return this.sensors;
  }

}
