import fs from "fs";

export async function writeFile(file,content){
  return new Promise((resolve,reject)=>{
    fs.writeFile(file,content,"utf-8",(err)=>{
      if(err){
        reject(err);
      }else{
        resolve();
      }
    });
  });
}

export async function readFile(file){
  return new Promise((resolve,reject)=>{
    fs.readFile(file,"utf-8",(err,content)=>{
      if(err){
        reject(err);
      }else{
        resolve(content);
      }
    });
  });
}

export async function readdir(folder){
  return new Promise((resolve,reject)=>{
    fs.readdir(folder,(err,files)=>{
      if(err){
        reject(err);
      }else{
        resolve(files);
      }
    })
  })
}

// fs.constants.R_OK | fs.constants.W_OK
export async function access(folder,permissions){
  return new Promise((resolve,reject)=>{
    fs.access(folder,permissions, (err) => {
      if(err){
        reject(err);
      }else{
        resolve();
      }
    });
  });
}

export async function fstat(file){
  return new Promise((resolve,reject)=>{
    fs.stat(file, (err,result) => {
      if(err){
        reject(err);
      }else{
        resolve(result);
      }
    });
  });

}
