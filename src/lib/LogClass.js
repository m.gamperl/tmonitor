import bunyan from "bunyan";

export class LogClass {

  constructor(config){    
    this.log = bunyan.createLogger({name : this.constructor.name, level : config.logLevel});
  }

}
