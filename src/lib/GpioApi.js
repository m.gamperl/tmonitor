var Gpio = require("gpio");
import {LogClass} from "./LogClass";
import _ from "lodash";

export class GpioApi extends LogClass {

  constructor(_config){
    super(_config);
    // Calling export with a pin number will export that header and return a gpio header instance
    this.config = _config;
    this.gpios = _config.gpios;
  }

  initGpios(){
    _.keys(this.gpios).forEach((pin)=>{
      var gpio = this.gpios[pin];

      this.log.info("initialize gpio: ", pin, " called ", gpio.name , " type ", gpio.type);
      this.gpios[pin].gpio = Gpio.export(4, {
             direction: gpio.type,
             interval: 200,
             ready: function() {
             }
          });

      this.gpios[pin].gpio.set(0);
    });
  }

  getGpios(){
    return this.gpios;
  }

  set(pin,value){
    var gpio = this.gpios[pin];
    gpio.gpio.set(value);
  }

  update(pin,data){
    this.gpios[pin].gpio.set(data.value);
    this.gpios[pin].name = data.name;
  }

  close(){
    _.keys(this.gpios).forEach((pin)=>{
      this.gpios[pin].gpio.unexport();
    });
  }

}
