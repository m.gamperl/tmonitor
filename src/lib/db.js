import _ from "lodash";
import {LogClass} from "./LogClass";
var PouchDB = require('pouchdb');

export class Db extends LogClass {

  constructor(_config){
    super(_config);
    this.config = _config;
    this.db = new PouchDB(this.config.database);
  }

  async storeData(data){
    return await this.db.put(data);
  }

  async createView(ddoc){
    //Creating couch db view for history records:
    // save the design doc
    return await this.db.put(ddoc);
  }

  async getAllHistoryRecords(){
    /*
    alternative? return await this.query("historyRecords/all");
    */
    return await this.query("historyRecords/all");
    /*try {
      var result = await this.db.allDocs({include_docs: true});
      return {
        status : "ok",
        result  : result
      };
    }catch(e){
      return {
        status : "error",
        error : e.name + ", "+ e.message
      }
    }*/
  }

  async getHistoryRecordsForLastMonth() {
    return await this.query("historyRecords/lastMonth");
  }

  async getHistoryRecordsForCurrentMonth() {
    return await this.query("historyRecords/currentMonth");
  }

  async query(view){
    try {
      var result = await this.db.query(view);
      return {
        status : "ok",
        result  : result
      };
    }catch(e){
      return {
        status : "error",
        error : e.name + ", "+ e.message
      }
    }
  }

  async clearDatabase(){
    try {
      await this.db.destroy();
      this.db = new PouchDB('tmonitor');
      return {
        "status" : "ok"
      };
    }catch(e){
      return {
        status : "error",
        error : e
      }
    }
  }

  async initDatabase(){

    try {
      var result = await this.db.viewCleanup();

      try {
        var historyRecordsDesign = await this.db.get("_design/historyRecords");
        this.log.info("removing view");
        var deleteResult = await this.db.remove(historyRecordsDesign);
      }catch(e){
        this.log.info("view does not exist yet");
      }finally {
        this.log.info("recreating view");
        await this.createHistoryRecordView();
      }

    }catch(e){
      throw e;
    }
  }

  async createHistoryRecordView(){
    await this.createView({
        _id: '_design/historyRecords',
        views: {
          all: {
            map: this.allHistoryRecords.toString()
          },
          lastMonth : {
            map : this.lastMonthHistoryRecords.toString()
          },
          currentMonth : {
            map : this.currentMonthHistoryRecords.toString()
          }
        }
      });
  }

  allHistoryRecords(doc){
    if (doc.type === "historyRecord") {
      emit(doc.datetime,doc.sensors);
    }
  }

  lastMonthHistoryRecords(doc){
    var lastMonth = (Number(new Date().getMonth())-1) < 0 ? 11 : (Number(new Date().getMonth())-1);
    if(doc.type === "historyRecord" && Number(new Date(doc.datetime).getMonth()) === lastMonth ){
      emit(doc.datetime,doc.sensors);
    }
  }

  currentMonthHistoryRecords(doc){
    if(doc.type === "historyRecord" && Number(new Date(doc.datetime).getMonth()) === Number(new Date().getMonth()) ){
      emit(doc.datetime,doc.sensors);
    }
  }

}
