import _ from "lodash";
import {LogClass} from "./LogClass";
import * as MathUtils from "./MathUtils";

export class Exporter extends LogClass {

  constructor(_config){
    super(_config);
    this.config = _config;
  }

  getMonth(date) {
      var month = date.getMonth() + 1;
      return month < 10 ? '0' + month : '' + month; // ('' + month) for string result
  }

  getDay(date){
    var day = date.getDate();
    return day < 10 ? '0' + day : ''+ day;
  }

  getNumber(n){
    return n < 10 ? '0'+ n : '' + n;
  }

  replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

  generateCSV(result,sensors){
    result.rows = result.rows.map((row)=>{
      var date = new Date(parseInt(row.id));
      row.values = _.map(_.values(row.value),"temperature").map((v)=>{
        return Math.round10(v, -1);
      });
      return  '"'+ this.getDay(date)+"."+ this.getMonth(date) + "." + date.getFullYear() + " " + this.getNumber(date.getHours())+ ":"+ this.getNumber(date.getMinutes()) + '";'+ this.replaceAll(row.values.join(";"),"\\.",",");
    });

    var rows = ["Datum;"+_.map(_.values(sensors),"name").join(";")].concat(result.rows);
    return rows.join("\r\n");
  }

  sendCSV(result,sensors,res){
    try {
      this.log.info("exporting result: ", result);
      var csvData = this.generateCSV(result,sensors);
      res.set("Content-Disposition","attachment;filename=historyRecords.csv");
      res.set("Content-Type","text/csv");
      res.send(csvData);
    }catch(e){
      res.json({
        status : "error",
        error : e.name + ", "+ e.message,
        stack : e.stack
      });
    }
  }

}
