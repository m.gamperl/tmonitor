import fs from "fs";
import path from "path";
import _ from "lodash";
import {LogClass} from "./LogClass";

export class Config extends LogClass {

  constructor(configFile,defaultConfigFile){
    super({logLevel : "warn"});
    this.configFile = configFile || path.resolve(process.cwd(),"./data/config.json");
    this.defaultConfigFile = defaultConfigFile || path.resolve(process.cwd(),"./defaults/config.json");
    this.loadConfig();
  }

  loadConfig(){
    this.config = this.loadConfigFromFile(this.configFile);
    this.defaultConfig = this.loadConfigFromFile(this.defaultConfigFile);

    if(!this.config){
      this.log.info("creating default config at ", this.configFile);
    }

    this.config = _.extend(this.config || {},this.defaultConfig);

    try {
      this.saveConfigToFile(this.config,this.configFile);
    }catch(e){
      this.log.error("can not write config to ", this.configFile);
      process.exit();
    }


    this.config = this.applyEnvironmentVariables(this.config,process.env);
    return this.config;
  }

  envVariableSet(k,env){
    var value = this.getEnvValue(k,env);
    return value !== null && value !== undefined && value.length > 0;
  }

  getEnvValue(key,env){
    return env["TMONITOR_"+key];
  }

  applyEnvironmentVariables(config,env){
    _.keys(config,(k)=>{
      if(this.envVariableSet(k,env)){
          config[k] = this.getEnvValue(k,env);
      }
    });
    return config;
  }

  saveConfigToFile(config,configFile){
    fs.writeFileSync(configFile,JSON.stringify(config,null,2));
  }

  loadConfigFromFile(configFile){
    try {
      fs.accessSync(configFile);
      return JSON.parse(fs.readFileSync(configFile));
    }catch(e){
      this.log.warn("can not read config file from ", configFile, e);
      return null;
    }
  }

  getConfig(){
    return this.config;
  }

  printConfig(){
    this.log.info(this.config);
  }

}
