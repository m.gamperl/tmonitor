<sensor style="text-align:center;">

  <div class="temperature">{temperature || opts.temperature}°C</div>
  <div onclick="{editName}" class="sensor-name">{opts.sensorName}<span role="button">&nbsp;&nbsp;<i aria-label="Namen ändern" onclick="{editName}" class="fa fa-pencil clickable"></i></span></div>

  editName(e){
    e.stopPropagation();
    var socket = io();
    var name = prompt("Name für "+opts.sensorName+" festlegen:",opts.sensorName);
    socket.emit("changeSensorName", {id : opts.sensorId, name : name});
    /*this.opts.sensorList.forEach(function(sensor,i){
      if(sensor.id === e.item.sensor.id){
        this.sensorList[i].name = name;
        this.update();
      }
    }.bind(this));*/
  }
</sensor>
