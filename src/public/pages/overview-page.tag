<overview-page>

  <!--<div class="gpio-container">
    <div each={gpio,i in gpioList} class="card-small">
      <gpio name="g{i}" gpio-id={gpio.id} gpio-name={gpio.name} gpio-value={gpio.value} />
    </div>
  </div>-->

  <div each={sensor,i in sensorList} class="card" >
    <sensor name="s{i}" sensor-id={sensor.id} sensor-name={sensor.name} temperature={sensor.temperature}/>
  </div>

  this.sensorList = [];
  this.gpioList = [];

  init() {
    var socket = io();
    this.socket = socket;

    socket.on('gpios',function(data){
      console.log(data.gpioList);
      this.gpioList = data.gpioList;
      this.update();
    }.bind(this));

    socket.on('sensors', function (data) {
      console.log(data.sensorList);
      this.sensorList = data.sensorList;
      this.update();
    }.bind(this));

    socket.on("newSensorName",function(data){
      this.sensorList.forEach(function(sensor,i){
        if(sensor.id === data.id){
          this.sensorList[i].name = data.name;
          this.update();
        }
      }.bind(this));
    }.bind(this));

    socket.on('newTemperature', function (data) {

      this.sensorList.forEach(function(sensor,i){
        if(sensor.id === data.id){
          this.sensorList[i].temperature = data.temperature;
          this.update();
        }
      }.bind(this));

    }.bind(this));

  }

  toggleEdit(){
    this.tags.s1.setTemp(11);
  }

  this.on('mount', function() {
    console.log("mount");
    this.init();
  })


</overview-page>
